---
id: download
title: Download Resources
sidebar_label: Download
---
This sandbox is meant to be the easiest way to get started with the SynchroniCity Framework. It helps you download and install the latest version, with examples that demonstrate how powerful it can be.

In order to make the deployment as easy and quick as possible, the platform architecure was defined with a container deployment approach using [Docker compose](https://docs.docker.com/compose/).

> Deployment works best on UNIX type environments such as **Linux** or **MacOS**. Support on Microsoft Windows is experimental, and should be avoided. **Linux** is the only recommended OS for production.

## Prerequisites
* Docker
    * Docker version 18.06.1 or newer
    * Docker-compose 1.22.0 or newer (is automaticall installed on MacOS and Windows)
    * Git
    * Internet connectivity

## Download the container configurations
The SynchroniCity project has made pre-configured versions of its components that you can play with out of the box. Of course you can customise this when the need arises, but getting started could not be easier.

```
git clone https://gitlab.com/synchronicity-iot/platform-deployment-docker.git --recurse-submodules
```
or if you do not have `git` installed you can browse to the GitLab website and download a zip file containing the code from [here](https://gitlab.com/synchronicity-iot/platform-deployment-docker).

Then you need to change to the latest available branch of the code, and navigate to the docker compose files. This can be accomplished with the following commands.

```
cd platform-deployment-docker/docker
```

The structure of files in the project is as follows.
```bash
root-directory
└── docker
    ├── baseline-ngsi
    │   └── compose-NGSI.yml
    ├── historical-ngsi
    │   ├── agent_his.conf
    │   ├── compose-historical.yml
    │   ├── config_API.js
    │   └── grouping_rules.conf
    └── security
    │   ├── compose-sec-idm.yml
    │   ├── compose-sec-pep.yml
    │   ├── compose-security.yml
    │   ├── config-pep-cb.js
    │   ├── config-pep-his.js
    │   └──config_idm.js
    └── fiware-idm
        ├── README.md
        ├── config.js.template
```

## Download all the docker images
To download all the required docker images from all the docker compose files, simply use the `docker-compose pull` command as demonstrated below.

### The Context Broker
```bash
docker-compose -f baseline-ngsi/compose-NGSI.yml pull
```

The output will be something like this:
```bash
Pulling mongo-cb ... done
Pulling orion-cb ... done
```

### Historical API and Service
```bash
docker-compose -f historical-ngsi/compose-historical.yml pull
```

The output will be something like this:
```bash
Pulling mongo-his    ... done
Pulling cygnus-ngsi  ... done
Pulling pm2_hist_api ... done
```

### Security Framework
> We will not cover the security framework in the getting started tutorial, so downloading it is not nessecary for the sandbox.

```bash
docker-compose -f security/compose-sec-idm.yml -f security/compose-sec-pep.yml pull
```

The output will be something like this:
```bash
Pulling mongo-pep ... done
Pulling mysql     ... done
Pulling keyrock   ... done
Pulling pep-his   ... done
Pulling pep-cb    ... done
```

## Optional extra's
There are some optional extra's that might improve your experience, but they are not required to follow the tutorial.
### jq
In the examples on the pages that follow, many commands against the system will be done using cURL. The output will most often be of type `JSON` which has been run through a 'beautifier'. The one used to generate the output is [https://stedolan.github.io/jq/](https://stedolan.github.io/jq/download/). Follow the link for detailed installation guides.

On MacOS with Homebrew
```bash
brew install jq
```

On Linux
```bash
sudo apt-get install jq
```

To use jq, you can simply add ` | jq .` to the end of the cURL examples, and the JSON will be formatted in a human readable format by *piping* it into jq. The example below will not work yet, as we havent started the services, it is just to show how you can optionally use jq to make the output more readable.
```bash
curl http://localhost:8082/v1/stats | jq .
```
