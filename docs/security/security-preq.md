---
id: security-preq
title: Installation Prerequisites
sidebar_label: Prerequisites
---
The security framework is a collection of components that provide identity management, authentication and role based access control. Before proceeding with the tutorial, please make sure the 'Getting Started' guide has been run. As with many things, this guide provides an overview of how to get the components running and integrated in a simulated environment on your local machine.

> Deployment to a production environment should be done with care, taking into consideration best practices and the specific needs of your organisation. **After following this tutorial you will have users configured with default credentials and this is not recommended in production.**

The tokens generated during this guide should be stored safely, and used for test and development only. Do not distribute keys from production environments publicly. This is meant to be a simple guide to setup a security framework for SynchroniCity components. For the full documentation please visit this site. [https://fiware-idm.readthedocs.io/en/latest/](https://fiware-idm.readthedocs.io/en/latest/)

## Prerequisite check
Make sure that you have followed the [Getting Started](gettingstarted/download.md) guide and have downloaded the repository containing the docker files, and started the real time context broker and the historical framework. A simple way to check if you have all the components ready on your local computer is to issue a request to the context broker using cURL.
```bash
curl http://localhost:1026/version
```
The output JSON will look like this:
```javascript
{
  "orion": {
    "version": "2.2.0",
    "uptime": "0 d, 0 h, 0 m, 31 s",
    "git_hash": "5a46a70de9e0b809cce1a1b7295027eea0aa757f",
    "compile_time": "Mon Feb 25 15:15:27 UTC 2019",
    "compiled_by": "root",
    "compiled_in": "37fdc92c3e97",
    "release_date": "Mon Feb 25 15:15:27 UTC 2019",
    "doc": "https://fiware-orion.rtfd.io/en/2.2.0/"
  }
}
```

Likewise you can check that the historical api's are running using the cURL command below:
```bash
curl http://localhost:8082/v1/stats
```
This should yield a JSON response like this:
```javascript
{
  "success": "true",
  "stats": {
    "sources": [
      {
        "name": "ORION-source",
        "status": "START",
        "setup_time": "2019-08-26T14:07:25.588Z",
        "num_received_events": 0,
        "num_processed_events": 0
      }
    ],
    "channels": [
      {
        "name": "mongo-channel",
        "status": "START",
        "setup_time": "2019-08-26T14:07:25.774Z",
        "num_events": 0,
        "num_puts_ok": 0,
        "num_puts_failed": 0,
        "num_takes_ok": 0,
        "num_takes_failed": 2
      }
    ],
    "sinks": [
      {
        "name": "SYNC-historical",
        "status": "START",
        "setup_time": "2019-08-26T14:07:25.646Z",
        "num_processed_events": 0,
        "num_persisted_events": 0
      }
    ]
  }
}
```

Congratulations. You are now ready to install the security framework.
