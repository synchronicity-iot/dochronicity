---
id: security
title: Installing the Security Framework
sidebar_label: Install Framework
---
Before proceeding with the tutorial, please make sure the [Getting Started](gettingstarted/download.md) guide has been run. As with many things, this guide provides an overview of how to get the components running and integrated in a simulated environment on your local machine.

> Make sure that you have read the [prerequisites](security-preq.md) and have run the [Getting Started](gettingstarted/download.md) guide.

## Commands for starting the security components
To start the identity manager Keyrock please issue the command below from inside the docker folder that came with the distribution.

```bash
docker-compose -f security/compose-sec-idm.yml up -d
```

It should output the following to the screen
```bash
Starting mysql-idm ... done
Starting idm-keyrock-7 ... done
```

To test that the component is actually running, you can issue the command below to check the status of the services.
```bash
docker-compose -f security/compose-sec-idm.yml ps
```

If everything is okay, you should see the message below:
```bash
    Name                   Command                  State                          Ports
------------------------------------------------------------------------------------------------------------
idm-keyrock-7   /opt/fiware-idm/docker-ent ...   Up             0.0.0.0:3000->3000/tcp, 0.0.0.0:443->443/tcp
mysql-idm       /entrypoint.sh --default-a ...   Up (healthy)   0.0.0.0:3306->3306/tcp, 33060/tcp
```

There is unfortunately a bug in the current deployment available on docker, so the database is not initiated. The commands below may be obsoleted later, when a fix becomes available.
```bash
docker exec -it idm-keyrock-7 npm run migrate_db
```
This command will execute a series of commands against the MySQL database, and output several hundred lines to the terminal. If successful the final lines should finish something like this:
```bash
...
== 20190429164755-CreateUsagePolicyTable: migrating =======
== 20190429164755-CreateUsagePolicyTable: migrated (0.030s)

== 20190507112246-CreateRoleUsagePolicyTable: migrating =======
== 20190507112246-CreateRoleUsagePolicyTable: migrated (0.027s)

== 20190507112259-CreatePtpTable: migrating =======
== 20190507112259-CreatePtpTable: migrated (0.028s)
```

The next command will insert / seed the database with some default values to enable the KeyRock service to function normally.
```bash
docker exec -it idm-keyrock-7 npm run seed_db
```

Likewise this command will output potentially hundreds of lines to the terminal, ending with something like this:
```bash
Loaded configuration file "config.js".
Using environment "database".
sequelize deprecated String based operators are now deprecated. Please use Symbol based operators for better security, read more at http://docs.sequelizejs.com/manual/tutorial/querying.html#operators node_modules/sequelize/lib/sequelize.js:242:13
== 201802190000-FillUserTable: migrating =======
== 201802190000-FillUserTable: migrated (0.015s)

== 201802190005-FillOAuthClientTable: migrating =======
== 201802190005-FillOAuthClientTable: migrated (0.003s)

== 201802190010-FillRoleTable: migrating =======
== 201802190010-FillRoleTable: migrated (0.004s)

== 201802190015-FillPermissionTable: migrating =======
== 201802190015-FillPermissionTable: migrated (0.006s)

== 201802190020-FillRolePermissionTable: migrating =======
== 201802190020-FillRolePermissionTable: migrated (0.004s)
```

## Getting your first authentication token
To test the service and get a token from the default admin user please issue the command below.
```bash
curl -i -X 'POST' -H 'Content-Type: application/json' -d '{"name": "admin@test.com", "password": "1234"}' 'localhost:3000/v1/auth/tokens'
```
This will output two things to the terminal, the header of the response containing the authentication token called **X-Subject-Token**:
```text
HTTP/1.1 201 Created
Cache-Control: no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0
X-Subject-Token: 853db849-8971-4e17-8177-7bea5a5ec325
Content-Type: application/json; charset=utf-8
Content-Length: 138
ETag: W/"8a-XVTTRiqhSPZ1EM5wvtMg6kLR2Nw"
Set-Cookie: session=eyJyZWRpciI6Ii8ifQ==; path=/; expires=Tue, 27 Aug 2019 16:43:46 GMT; httponly
Set-Cookie: session.sig=7Yig_XPmOCDlLaBJE9oxfRT2zBM; path=/; expires=Tue, 27 Aug 2019 16:43:46 GMT; httponly
Date: Tue, 27 Aug 2019 15:43:46 GMT
Connection: keep-alive
```

The payload will contain a JSON with some metadata about the token, for example expiry time etc.
```javascript
{
  "token": {
    "methods": [
      "password"
    ],
    "expires_at": "2019-08-27T16:43:46.986Z"
  },
  "idm_authorization_config": {
    "level": "basic",
    "authzforce": false
  }
}
```
