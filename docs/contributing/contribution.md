---
id: contribution
title: Contributing to SynchroniCity GitLab Repo
sidebar_label: Contribute Code
---
It is possible to contribute to the SynchroniCity project repsitories using merge requests. It is a simple process where you create a copy of the project you want to contriute to, and ask us to merge the changes into our main repository.

## Forking and cloning a project
To contribute to the SynchroniCity GitLab, first navigate to the project you want to contribute to, and make a fork of it.

- To make a fork simple click on __Fork__ in the top-right corner of the page, as shown in the image below (Marked by a red box):
![Marked Fork Button](assets/contributing/forkButton.jpg)

- Then select the namespace to save the fork to.

Once you have a fork of the project, you need to *clone* the project.
- To `clone` the project click on __Clone__ next to the __Fork__ button, and copy the HTTPS URL (or the SSH URL if you have set up SSH on GitLab).
- Then open a terminal where you want to download the project, and run the following command:

> Replace the `COPIED-URL` with the URL you copied before.

```bash
git clone COPIED-URL
```

You can now make changes to the project.

## Push changes to the project
When you are done making changes to the project, you need to commit and push your changes.

First open up the project folder in the terminal. This can be done in the terminal used previously by `cd` into the project folder:

> Replace `PROJECT-NAME` with the name of the project you cloned

```bash
cd PROJECT-NAME
```

- To commit you first need to `add` the files you want to commit, you can add all changes at once with:

```bash
git add .
```

- Then commit with the following command:

> Replace `COMMIT-MESSAGE` with a description o the changes or the intention of the commit

```bash
git commit -m "COMMIT-MESSAGE"
```

- Then push the commit:

```bash
git push origin master
```

## Making a Merge Request to the SynchroniCity project
To make a merge request, open up your forked project and go to __Merge Requests__ on the left side of the page.

On the merge request page, click on __New merge request__ to open a new Merge Request.

Make sure that the correct Source branch and target branch is selected. An example of this can be seen below:
![New Merge Request](assets/contributing/newMergeRequest.PNG)
- The Source branch is your forked project and the branch you pushed your changes to. (In the example above the changes was pushed to a development branch instead of master)
- And the target branch is the project you forked from (make sure that the correct branch is selected).

Next click on the __Compare brances and continue__ button. On the next page at a minimum add a Title and Description to your merge request.

When ready click on the __Submit merge request__ button.

Congratulation! You have now submitted a merge request
