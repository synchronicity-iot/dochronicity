---
id: highavailability
title: High Availability Setup
sidebar_label: High Availability
---
This tutorial will demonstrate that the Context Management API's can easily be deployed in a 'High Availability', setup that can provide added redundancy and higher performance. It will be a demonstration using Docker Swarm technology, and only focus on the Context Management API's and not the underlying database. Deploying a MongoDB is out of scope of this guide, but managed MogoDB instances can be used with this tutorial, or can be configured by following tutorials online e.g. [medium.com](https://medium.com/@gjovanov/mongo-docker-swarm-fully-automated-cluster-9d42cddcaaf5) or [cloud.mongodb.com](https://cloud.mongodb.com).

## Docker Swarm
Setting up nodes in a Docker Swarm cluster is beyond the scope of this tutorial, but the [docker website](https://docs.docker.com/engine/swarm/) has excellent tutorials on this. It is assumed that you have access to a docker swarm master node, and this can be tested by issuing the following command on your cluster.

```bash
docker node ls
```

it you are on a Swarm Master node a health cluster could look like this:
```bash
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
zuf5x23otp4y2jjciughyh025     hyper038            Ready               Active                                  19.03.5
auouuclvmpzt7ra7s7pv31doi     hyper039            Ready               Active                                  19.03.5
4xuji4cr0hx2nhea1v1zthozc     hyper040            Ready               Active                                  19.03.1
nnfaglnwudp4kt9gq5d96rgvl *   swarm-master        Ready               Active              Leader              19.03.1
```
If you encounter an error such as the following:
```bash
Error response from daemon: This node is not a swarm manager. Worker nodes cant be used to view or modify cluster state. Please run this command on a manager node or promote the current node to a manager.
```
then you need to follow up wit the documentation for setting up a Docker Swarm Cluster.

### MongoDB Prerequisites
The Orion Context Broker requires a MongoDB database to run, and for this tutorial we are using a cloud hosted MongoDB instance.
> **Please note**. The Orion Context Broker used in this tutorial **DOES NOT** support version **4.0** or above. It is possible to instantiate a **MongoDB 3.6.15** on the [cloud.mongodb.com](https://cloud.mongodb.com) site, and it will be used throughout this tutorial.

> **Please Note**. The connection string must comply to the _old_ MongoDB 3.4 standard due to the [legacy version](http://mongocxx.org/legacy-v1/) of C++ drivers used by the Orion Context Broker.

You can test your connection string using this example in a terminal window where the mongo-cli is installed, just replace URI, username and password with values matching your setup.
```bash
mongo "mongodb://cluster0-shard-00-00-60238.mongodb.net:27017,cluster0-shard-00-01-60238.mongodb.net:27017,cluster0-shard-00-02-60238.mongodb.net:27017/test?replicaSet=Cluster0-shard-0" --ssl --authenticationDatabase admin --username <username> --password <password>
```

### Service Definition
Deploying the Orion Context Broker is extremely simple, as it is simply a binary that runs while connected to a MongoDB. To start a service with one replica, the following command could be issued:
```bash
docker service create --replicas 1 --name orion-cb   --publish 1026:1026 --entrypoint "/usr/bin/contextBroker" fiware/orion:2.3.0 -fg -dbhost cluster0-shard-00-00-60238.mongodb.net:27017,cluster0-shard-00-01-60238.mongodb.net:27017,cluster0-shard-00-02-60238.mongodb.net:27017 -rplSet Cluster0-shard-0 -dbuser <username> -dbpwd <password> -dbAuthDb admin -dbSSL -db test
```
 It is now easy to scale the `orion-cb` service to run multiple instances against the MongoDB cluster:
 ```bash
 docker service scale orion-cb=3
 ```
To check the services converge, issue the command below.
```bash
docker service ps orion-cb
```
The output should look like this:
```bash
ID                  NAME                IMAGE                NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
fx1d7s3nd88g        orion-cb.1          fiware/orion:2.3.0   swarm-master        Running             Running 4 minutes ago
y73zogam7ora        orion-cb.2          fiware/orion:2.3.0   hyper038            Running             Running 11 seconds ago
1s8ylotu37va        orion-cb.3          fiware/orion:2.3.0   hyper039            Running             Running 11 seconds ago
```

## Disclaimer
This is an example to demonstrate that the Orion Context Broker can run in a 'High Availability' setup. Actual deployment in a production environment should as always be carefully considered. Both Docker Swarm and Kubernetes are viable solutions, here we have focused on demonstrating Docker Swarm.