---
id: atomicservice
title: Atomic Service Example
sidebar_label: Atomic Service
---
The SynchroniCity project has produced a catalogue of atomic services. They act as standalone components on top of the base framework to be used as building blocks for the cities services. Atomic services can be used as a standalone or in composition for achieving more advanced backend service for the final city service. The nature of the atomic services facilitates reference zone customization and stimulates replication based on common solutions and designs. These common solutions prevent both “vendor lock-in” from the cities’ perspective and “city lock-in” from the perspective of city service and middleware providers. All the atomic services are delivered in a way that are easily customized.

> **Please note** The guide blow demonstrates two specific atomic services. For more details and a list of available atomic services please visit the website [http://synchronicity-iot.eu](https://synchronicity-iot.eu/tech/atomic-services/).

> **Please note** The guides below demonstrate how to get started with some atomic services. For production environments, plase take into consideration your own deployment needs, credentials, backup etc.

### Docker
Although the source code for all atomic services is provided to be compiled and easily extended, the SynchroniCity projecty also provides easily consumed Docker containers that can be linked to existing deployments and provide functionality out of the box. To find out more, visit the [hub.docker.com](https://hub.docker.com/orgs/synchronicityiot/repositories) website.

## Trackmap Atomic Services
The plugin extends the functionality of [Grafana](https://grafana.com/grafana/plugins/alexandra-trackmap-panel) to be able to visualize NGSIv2 data stored in [Quantum Leap](https://quantumleap.readthedocs.io/en/latest/) on a map, in three visualization styles. For more information on how to install and configure Quantum Leap, there is a guide available here: [https://quantumleap.readthedocs.io](https://quantumleap.readthedocs.io/en/latest/admin/).

![screenshot](https://gitlab.com/synchronicity-iot/grafanatrackmapatomicservice/raw/master/images/trackMap.jpg)

### Install Grafana and Plugin
To easily start an instance of grafana on your local machine, please issues the following command:
```bash
docker run --rm -it -p 3000:3000  -e "GF_INSTALL_PLUGINS=alexandra-trackmap-panel" grafana/grafana
```

Once started you can log in to the grafana portal [http://localhost:3000](http://localhost:3000) using the default credentials **admin/admin**. You will be asked to change the admin password on your first login attempt.

### Configure Datasource


### Visualizing Query

> **Please Note** For more information about running grafana in docker, please visit the [grafana](https://grafana.com/docs/grafana/latest/installation/docker/#installing-plugins-from-other-sources) website.

## Data Transformation