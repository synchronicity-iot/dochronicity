---
id: baselinengsi
title: The Baseline NGSI Service
sidebar_label: Baseline NGSI
---
Context Management APIs represent a standard way to communicate with the Context Management module, a key component of the SynchroniCity architecture. The main concept behind managing of data and information in SynchroniCity, is the adoption of an abstraction level that represents any information related to the real world (e.g. sensors data) as a “context entity”. characterised by specific attributes and metadata. The logical model of these interfaces is based to NGSI meta model, which offers a powerful way to model the real world, in order to extract high quality information from observation of phenomena.
## Examples
For the full documentation and exhaustive examples, please look at the full documentation at Apiary: [https://synchronicityiot.docs.apiary.io](https://synchronicityiot.docs.apiary.io)
The examples here are thoroughly tested and give a taste of what can be accomplised with the Context Broker.
### Publish
First let us create an entity, if you have not already done so in the [Getting Started Section](../gettingstarted/runsandbox)
> **Please note**. The Context Broker **POST** method is not [idempotent](https://en.wikipedia.org/wiki/Idempotence). This means a POST can only be done **ONCE** for each unique entity.

```bash
curl -X "POST" "http://localhost:1026/v2/entities?options=keyValues" \
  -H 'Content-Type:application/json' \
  -d $'{
    "id": "vehicle:WasteManagement:black-box",
    "type": "Vehicle",
    "category": {
      "value": [
        "municipalServices"
      ]
    },
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [
          56.18786,
          10.16818
        ]
      },
      "metadata": {
        "timestamp": {
          "type": "DateTime",
          "value": "2019-01-21T06:36:34.766099026Z"
        }
      }
    },
    "name": {
      "value": "vehicle:WasteManagement:black-box"
    },
    "refDevice": {
      "type": "Text",
      "value": "urn:ngsi-ld:Device:70B3D54997587419",
      "metadata": {}
    },
    "refVehicleModel": {
      "type": "Relationship",
      "value": "vehiclemodel:econic"
    },
    "serviceProvided": {
      "value": [
        "garbageCollection",
        "wasteContainerCleaning"
      ]
    },
    "vehicleType": {
      "value": "lorry"
    }
  }'
```

> If you experience a `HTTP 422 Unprocessable Entitiy` error or a message containing `"description": "Already Exists"`, it is likely because the entity was already created.

You should now be able to make an HTTP GET request to retrieve the entire entity, or just a single attrubute:

```bash
curl "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box/attrs/location?options=keyValues" \
     -H 'Accept: application/json'
```
Will output the current `location` attribute to the screen:
```json
{
  "location": {
    "type": "geo:json",
    "value": {
      "type": "Point",
      "coordinates": [
        56.18786,
        10.16818
      ]
    },
    "metadata": {
      "timestamp": {
        "type": "DateTime",
        "value": "2019-01-21T06:36:34.766099026Z"
      }
    }
  }
}
```

### Update
Let us now update the `location` attribute using the `HTTP PATCH` command:
```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box/attrs?options=keyValues" \
     -H 'Content-Type: application/json' \
     -d $'{
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [
              55.659821,
              12.590755
            ]
          },
          "metadata": {
            "timestamp": {
              "type": "DateTime",
              "value": "2019-01-21T08:30:00.000000000Z"
            }
          }
        }
      }'
```
A subsequent `HTTP GET` request to the entity or attribute will show the update value for `location`.
```json
{
  "location": {
    "type": "geo:json",
    "value": {
      "type": "Point",
      "coordinates": [
        55.659821,
        12.590755
      ]
    },
    "metadata": {
      "timestamp": {
        "type": "DateTime",
        "value": "2019-01-21T08:30:00.000000000Z"
      }
    }
  }
}
```

### Subscribe
For the full documentation and exhaustive examples, please look at the full documentation at Apiary: [https://synchronicityiot.docs.apiary.io](https://synchronicityiot.docs.apiary.io)
The examples here are thoroughly tested and give a taste of what can be accomplised with the Context Broker.

Go the the website [https://requestbin.fullcontact.com](https://requestbin.fullcontact.com) and press `Create a Requestbin`. This will generate a URL that you must save, and insert into the below JSON. For example `http://requestbin.fullcontact.com/1jz4tvp1`.

> The Context Broker uses `HTTP POST` requests to notify subscribers of upated values. This requires you to implment an HTTP server locally to test subscriptions or us an online 'bin'. For testing locally the recommended system is [https://github.com/Runscope/requestbin](https://github.com/Runscope/requestbin) which can also be found as a docker container [https://hub.docker.com/r/agaveapi/requestbin/](https://hub.docker.com/r/agaveapi/requestbin/). An online alternative can be found here: [https://requestbin.fullcontact.com](https://requestbin.fullcontact.com). Both have been tested with the Context Broker.

For our first subscription, lets get notified when the `location` attribute changes.
```bash
curl -X "POST" "http://localhost:1026/v2/subscriptions" \
     -H 'Content-Type: application/json' \
     -d $'{
  "description": "My very first SynchroniCity subscription",
  "subject": {
    "entities": [
      {
        "type": "Vehicle",
        "idPattern": ".*"
      }
    ],
    "condition": {
      "attrs": [
        "location"
      ]
    }
  },
  "notification": {
    "http": {
      "url": "http://requestbin.fullcontact.com/1k1dd4y1"
    },
    "attrs": [
      "locatin"
    ]
  },
  "expires": "2020-04-05T14:00:00.00Z",
  "throttling": 5
}'
```
This is the most simple example, and more can be found by following the link to the documentation. The gist of it that we create a subscriber that listens to all entities where a change to `location` occours. The query sends the updated `location` to the URL of the subscribing server. In the case above it is `http://requestbin.fullcontact.com/1k1dd4y1`.

> **NOTICE!!!** The `notification.http.url` of the JSON above is very important. It can be an external service on the WWW, or an internal service run on your local machine. In the case of a local service, make sure to use the DNS name provided through the Docker network. If it a local service it could be for example `http://bin.app:8000/y6vfdzy6`

Now if you update the `location` with the cURL example below, the service endpoint you specified will be notified.
```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [-10, 10]
      },
      "metadata": {
        "timestamp": {
        "type": "DateTime",
        "value": "2019-01-11T12:00:10"
        }
      }
    }
  }'
```

You can get the list of subscriptions with the following HTTP GET:
```bash
curl "http://localhost:1026/v2/subscriptions&options=keyValues"
```

The output JSON will look like this:
```json
[
  {
    "id": "5c45bc2b93b149326432aff3",
    "description": "My very first SynchroniCity subscription",
    "expires": "2020-04-05T14:00:00.00Z",
    "status": "active",
    "subject": {
      "entities": [
        {
          "idPattern": ".*",
          "type": "Vehicle"
        }
      ],
      "condition": {
        "attrs": [
          "location"
        ]
      }
    },
    "notification": {
      "attrs": [
        "locatin"
      ],
      "attrsFormat": "normalized",
      "http": {
        "url": "http://requestbin.fullcontact.com/1k1dd4y1"
      }
    },
    "throttling": 5
  }
]
```

Let us now delete the subscription to the Requestbin
> **NOTICE!!** The last part of the cURL below need to be changed to the `id` of the subscription you are trying to delete.

```bash
curl -X "DELETE" "http://localhost:1026/v2/subscriptions/5c45bc2b93b149326432aff3"
```

If you now make a HTTP GET to the subscription, it should return a empty list.

Congratulations.
