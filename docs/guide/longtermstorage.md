---
id: longtermstorage
title: Long Term Historical NGSI
sidebar_label: Historical NGSI
---
The Long Term Historical NGSI service is able to store previous versions of attributes and entities, by listening subscribing to the Context Broker through the Cygnus component. It also provides easy access though a REST interface. For detailed information about the Cygnus component and all the configuration options available please go to [https://fiware-cygnus.readthedocs.io](https://fiware-cygnus.readthedocs.io). For detailed information about the REST interface, please visit the Apiary site on [https://synchronicityiot.docs.apiary.io](https://synchronicityiot.docs.apiary.io).

## Setup
The Long Term Historical NGSI service needs to subscribe to notifications just like any other service. Following this guide will let you subscribe to notifications to `location` entities.

> NOTICE!!! It is important that you do not subscribe to `wildcard` entities and `wildcard` attributes. This will not scale in a production environment.

Create a subscription with the following command:
```bash
curl -X "POST" "http://localhost:1026/v2/subscriptions" \
  -H 'Content-Type: application/json' \
  -d $'{
    "subject": {
      "condition": {
        "attrs": [
          "location"
        ]
      },
      "entities": [
        {
          "type": "Vehicle",
          "idPattern": ".*"
        }
      ]
    },
    "description": "A subscription to location data",
    "notification": {
      "attrs": [
        "location"
      ],
      "http": {
        "url": "http://cygnus-ngsi.docker:5050/notify"
      },
      "attrsFormat": "legacy"
    },
    "expires": "2020-04-20T14:00:00.00Z"
  }'
```
> Notice here that the `notification.http.url` references the internal DNS name of the Cygnus component. There is no need to change anything if you are running the *SandBox* experience. If your Cygnus component is hosted elsewhere, then please change it.

Once your subscription is activated, you can try to update the `location` of an entity. For more information please refer to the [The Baseline NGSI Service Guide](baselinengsi).

```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [-10, 10]
      },
      "metadata": {
        "timestamp": {
        "type": "DateTime",
        "value": "2019-01-21T12:00:00"
        }
      }
    }
  }'
```
This will automatically trigger an update of the data stored in the historical database.

> To subscribe to other entity types, you also need to make a mapping configuration in the `grouping_rules.conf` file located in the directory of the historical api docker files. This file is mounted inside the container, and is settings are persistant. More detailed information can be found [HERE](https://fiware-cygnus.readthedocs.io/en/latest/cygnus-ngsi/installation_and_administration_guide/grouping_rules/index.html)

## Using the Historical API
The Historical API is extremely powerfull and the full documentation can be found at the Apiary website []().

Let us change a value with the `PATCH` command as we have done before:
```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:black-box/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [-12, 12]
      },
      "metadata": {
        "timestamp": {
        "type": "DateTime",
        "value": "2019-01-21T12:12:00"
        }
      }
    }
  }'
```

An example of selecting the changes to `location` can be found here:
```bash
curl "http://localhost:8080/v2/entities/vehicle:WasteManagement:black-box/attrs/location?time=2018-10-10&timerel=between&offset=0&limit=100&endtime=2020-12-31&type=Vehicle&options=keyValues"
```

The output will be similar to this:
```json
{
  "id": "vehicle:WasteManagement:black-box",
  "type": "Vehicle",
  "location": [
    {
      "type": "StructuredValue",
      "value": "{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-10,10]},\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2019-01-21T12:00:00\"}}}",
      "modifiedAt": "2019-01-21T13:23:26.030Z"
    },
    {
      "type": "StructuredValue",
      "value": "{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-12,12]},\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2019-01-21T12:12:00\"}}}",
      "modifiedAt": "2019-01-21T13:26:58.274Z"
    }
  ]
}
```

Congratulations
