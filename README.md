# Dochronicity

[![license](https://img.shields.io/badge/license-MIT-green.svg)](./LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> Dochronicity is using Docusaurus for easily deploying, and maintaining documentations.

Guides to Docusaurus can be found [here](https://docusaurus.io/docs/en/installation).

## Install / Run
Clone the repository or download it from the gitlab.
```
git clone https://gitlab.com/synchronicity-iot/dochronicity.git
```
Then cd to the website folder.
```
cd dochronicity/website
```
Install dependencies with either yarn or npm.
```
yarn install
```
Start the project.
```
yarn start
```
If this doesn't automatically open `localhost:3000`, open it manually.

### Docker
To run the project in docker, cd back to the root of the project, and first build it:
```
docker build -t synchronicityiot/docusaurus .
```
And then run it:
```
docker run --rm --name dochronicity -p 3000:3000  synchronicityiot/docusaurus
```
Open `localhost:3000`

## Contributing

A guide on how to contribute can be found [here](https://docs.synchronicity-iot.eu/docs/contributing/contribution)

## License

[MIT © Alexandra Institute.](./LICENSE)
