FROM node:8.11.4

WORKDIR /app/website

ADD ./website/package*.json ./
RUN yarn install

EXPOSE 3000 35729
COPY ./docs /app/docs
COPY ./website /app/website

CMD ["yarn", "start"]
